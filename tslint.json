{
    "extends": [
        "tslint-react",
        "tslint-sonarts",
        "tslint:all"
    ],
   "rulesDirectory": [
        "tslint-consistent-codestyle",
        "node_modules/tslint-clean-code/dist/src"
    ],
    "rules": {
        //// ts-lint overrides.
        "arrow-return-shorthand": false,
        "align": [true, "statements"],  // use inferred type of multiline (object) arguments
        "await-promise": [true, "Bluebird"],  // sequelize uses Bluebird
        "ban-types": {
            "options": [
                ["Object", "Avoid using the `Object` type. Did you mean `object`?"],
                ["Boolean", "Avoid using the `Boolean` type. Did you mean `boolean`?"],
                ["Number", "Avoid using the `Number` type. Did you mean `number`?"],
                ["String", "Avoid using the `String` type. Did you mean `string`?"],
                ["Symbol", "Avoid using the `Symbol` type. Did you mean `symbol`?"]
            ]
        },
        "ban": [true, "require"],
        "comment-format": [ true, "check-space"],  // Don't check uppercase.
        "completed-docs": false,  // Code has to be readable itself without any docs.
        "curly": [true, "ignore-same-line"],  // Quick conditional oneliners are ok.
        "file-name-casing": [true, "camel-case", "pascal-case"],  // Both are ok.
        "import-blacklist": [true,
            "@factury/core",  // Not to drag NodeJS stuff into front-end, but backend needs it nevertheless.
            "bignumber.js",  // Use only BN provided from common.
            "moment"  // Use only momentUtc and Moment provided from common.
        ],
        "indent": [true, "spaces", 4],
        "invalid-void": false,  // https://github.com/palantir/tslint/issues/4834
        "match-default-export-name": false,  // Doesn't work with Material-UI icons.
        "max-line-length": [true, {"limit": 150, "ignore-pattern": "(extends AbstractComponent(Routed)?<|export default wrapHOCs<)"}],
        "newline-per-chained-call": false,  // Opnionated style, don't spam LOC.
        "no-async-without-await": false,  // Because there's a functional difference where error gets thrown.
        "no-consecutive-blank-lines": [true, 2],  // Leave doubles alone to divide blocks, e.g. after imports.
        "no-console": [true, "debug", /* "info",  */"log", "time", "timeEnd", "trace"],  // TODO: get logger library.
        "no-default-export": false,  // Conflicts with React structure.
        "no-empty-interface": false,  // Because lots of Components has no props, but consistent typing is important.
        "no-implicit-dependencies": [true, "dev"],  // Because webpack requires to have them under devDependencies.
        "no-import-side-effect": false,  // Doesn't work with BluebirdHijack, Storybook and Webpack's plain CSS imports.
        "no-magic-numbers": false,  // TODO: Too much refactor.
        "no-namespace": false,  // TODO: investigate?
        "no-null-keyword": false,  // undefined means missing, null means explicit voidness.
        "no-require-imports": false,  // Special for 'electron-window-state'.
        "no-string-literal": false,
        "no-submodule-imports": [true,
            "@factury/platform-reactapp",  // That's very internal anyway.
            "@factury/core/dist/constants",  // Not to drag NodeJS stuff into front-end.
            "@factury/core/dist/interfaces",  // Not to drag NodeJS stuff into front-end.
            "@material-ui/core",
            "@material-ui/styles",
            "ol",  // As per their examples.
            "rxjs",  // As per guidelines for tree-shaking.
            "web3",
            "world-atlas",
            "world-countries",
            "react-dev-utils",  // SSR needs those "internals".
            "webpack-hot-middleware/client",  // SSR needs those "internals".
            "react-jss",  // SSR needs those "internals".
            "react-dom"  // SSR needs those "internals".
        ],
        "no-unnecessary-qualifier": false,  // Conflict with Babel loader that doesn't work quite like that.
        "no-unused-variable": false,  // Deprecated.
        "no-use-before-declare": false,  // Allow to define helpers below Component.
        "no-void-expression": false,  // Doesn't work with React's oneliner callbacks.
        "object-literal-shorthand": true,  // object-literal-shorthand offers an abbreviation not an abstraction
        "object-literal-sort-keys": [true, "ignore-case"],
        "ordered-imports": [true, {"named-imports-order": "any"}],  // TSHero sorts his own way.
        "prefer-function-over-method": [true, "allow-public"],  // Because React's render.
        "prefer-readonly": false,  // DOn't spam readonly at arrow function in React components.
        "quotemark": [true, "single"],
        "return-undefined": false,  // Conflicts with "no-return-undefined.
        "semicolon": [true, "never"],  // JS can be written cleanly.
        // "strict-boolean-expressions": [true, "allow-null-union", "allow-undefined-union"],
        "strict-type-predicates": false,  // Requires `--strictNullChecks`.
        "type-literal-delimiter": false,  // Don't enforce semicolons.
        "typedef": false,  // Don't spam typedefs.
        "variable-name": false,  // Conflicts with "naming-convention".
        "whitespace": [true,
            // "check-branch",  // "if(...)" is better than "if (...)", just like any normal function.
            "check-decl",
            "check-operator",
            "check-module",
            "check-separator",
            "check-type",
            "check-typecast",
            "check-preblock",
            "check-type-operator",
            "check-rest-spread"
        ],

        //// tslint-react overrides.
        "jsx-boolean-value": [true, "never"],
        "jsx-alignment": false,


        //// tslint-sonarTS overrides.
        // None so far.


        //// tslint-consistent-codestyle rules. Set explicitly because no recommends were provided.
        "early-exit": [true, { "max-length": 4 }],
        "naming-convention": [true,
            // forbid leading and trailing underscores and enforce camelCase/PascalCase on EVERY name. will be overridden by subtypes if needed
            {"type": "default", "format": ["camelCase", "PascalCase"], "leadingUnderscore": "forbid", "trailingUnderscore": "forbid"},
            {"type": "property", "modifiers": ["private", "var"], "leadingUnderscore": "allow"},
            // require all global constants to be camelCase or UPPER_CASE
            // all other variables and functions still need to be camelCase
            {"type": "variable", "modifiers": ["global", "const"], "format": ["camelCase", "PascalCase", "UPPER_CASE"]},
            // override the above format option for exported constants to allow only UPPER_CASE
            {"type": "variable", "modifiers": ["export", "const"], "format": "UPPER_CASE"},
            // require exported constant variables that are initialized with functions to be camelCase
            {"type": "functionVariable", "modifiers": ["export", "const"], "format": "camelCase"},
            // allow leading underscores for unused parameters, because `tsc --noUnusedParameters` will not flag underscore prefixed parameters
            // all other rules (trailingUnderscore: forbid, format: camelCase) still apply
            {"type": "parameter", "modifiers": "unused", "leadingUnderscore": "allow"},
            // forbid leading underscores for private properties and methods because TS already has modifier, all other rules still apply
            {"type": "member", "modifiers": "private", "leadingUnderscore": "forbid"},
            // same for protected
            {"type": "member", "modifiers": "protected", "leadingUnderscore": "forbid"},
            // exclicitly disable the format check only for method toJSON
            {"type": "method", "filter": "^toJSON$", "format": null},
            // enforce UPPER_CASE for all public static readonly(!) properties
            {"type": "property", "modifiers": ["public", "static", "const"], "format": "UPPER_CASE"},
            // enforce PascalCase for classes, interfaces, enums, etc. Remember, there are still no underscores allowed.
            {"type": "type", "format": "PascalCase"},
            // abstract classes must have the prefix "Abstract". The following part of the name must be valid PascalCase
            {"type": "class", "modifiers": "abstract", "prefix": "Abstract"},
            // interface names must start with "I". The following part of the name must be valid PascalCase
            {"type": "interface", "prefix": "I"},
            // generic type parameters must start with "T"
            // most of the time it will only be T, which is totally valid, because an empty string conforms to the PascalCase check
            // By convention T, U and V are used for generics. You could enforce that with "regex": "^[TUV]$" and if you are care that much for performance, you could disable every other check by setting a falsy value
            {"type": "genericTypeParameter", "prefix": "T"},
            // enum members must be in PascalCase. Without this config, enumMember would inherit UPPER_CASE from public static const property
            {"type": "enum", "format": "UPPER_CASE"},
            {"type": "enumMember", "format": "UPPER_CASE"}
        ],
        "no-accessor-recursion": true,
        "no-as-type-assertion": false,
        "no-collapsible-if": true,
        "no-else-after-return": true,
        "no-return-undefined": true,
        "no-static-this": true,
        "no-unnecessary-else": true,
        "no-unnecessary-type-annotation": true,
        // "no-unused": [true, "ignore-parameters"],
        "no-var-before-return": true,
        "object-shorthand-properties-first": false,  // Conflicts with "object-literal-sort-keys"
        "parameter-properties": true,
        "prefer-const-enum": true,
        "prefer-while": true,


        //// tslint-clean-code rules. Set explicitly because recommends overrides a lot tslint rules.
        "id-length": [true, {"min": 2, "max": 28, "exceptions": [
                // because "componentWillReceiveProps".length === 25
                // ... and "HotModuleReplacementPlugin".length === 26
                "d", "g", "r", // svg variables
                "T", "p", "a", "h",    // exceptions for T generic type and HTML tags,
                "x", "y", "z", "X", "Y", "Z",  // well-known coordinate variables.
                "t",  // i118n library.
                "_",  // well-known for use to "trash" parameter.
                "i",  // well-known "index" variable.
                "b",  // well-known for use in `array.sort((a, b)=>a-b)`.
                "k",  // d3 transform property.
                "IBondSyndicationPledgeInstance", // because the name is reexported
                "IBondSyndicationPledgeAttrsJson" // because the name is reexported
            ]}],
        "max-func-args": [true, 4],  // 4 according to Express framework.
        "min-class-cohesion": [true, 0.5],
        "newspaper-order": false,  // Conflicts with "member-ordering".
        "no-commented-out-code": false,  // Conflicts with "no-commented-code".
        "no-complex-conditionals" : true,
        "no-feature-envy": false,  // Doesn't work together with JSX html elements, nor deconstructors.
        "no-flag-args" : false,  // Because `handleChange(value: boolean)` from Material-UI philosophy.
        "no-for-each-push" : true,
        "no-map-without-usage" : true,
        "prefer-dry-conditionals" : true,
        "try-catch-first" : false,  // Forbids readable code, e.g. try-catch it outside of fn. Take care yourself.


        // TODO: Big work, later.
        "strict-boolean-expressions": false         //     21
    }
}
