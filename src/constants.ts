import Projection from 'ol/proj/Projection';
import { getTransform } from 'ol/proj';

export const EARTH_RADIUS = 6371008.8
export const fromProjection = new Projection({code: "EPSG:4326"});   // Transform from WGS 1984
export const toProjection   = new Projection({code: "EPSG:900913"}); // to Spherical Mercator Projection
export const myTransform = getTransform(fromProjection, toProjection) as (c1: [number, number]) => [number, number]
